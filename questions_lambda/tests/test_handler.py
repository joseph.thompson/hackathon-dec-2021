from http import HTTPStatus
import unittest
from unittest.mock import Mock, patch

from botocore.exceptions import ClientError

from questions_lambda import handler


AUTHZ_AUTHORIZED_MESSAGE = 'Authorized'
AUTHZ_UNAUTHORIZED_MESSAGE = 'Unauthorized'


class HandlerTestCase(unittest.TestCase):
    def setUp(self):
        self.db_greeting_msg = u'Test Greeting'
        handler.table = Mock(get_item=Mock(return_value={
            'Item': {'Greeting': self.db_greeting_msg}
        }))

    def _mock_response(self, status=None, json_data=None):
        mock_resp = Mock()
        mock_resp.status_code = status
        mock_resp.json = Mock(return_value=json_data)
        return mock_resp

    @patch('requests.get')
    def test_handler__returns_success(self, mock_get):
        result = handler.handler({}, Mock())
        self.assertEqual(result, {'statusCode': HTTPStatus.OK, 'body': self.db_greeting_msg})

    @patch('requests.get')
    def test_handler_when_db_error__returns_success_with_default_msg(self, mock_get):
        mock_query = Mock()
        mock_query.side_effect = ClientError({
            'Error': {}
        }, 'mock_get_item')
        handler.table = Mock(get_item=mock_query)
        result = handler.handler({}, Mock())
        self.assertEqual(result, {'statusCode': HTTPStatus.OK, 'body': handler.DEFAULT_GREETING_MSG})


if __name__ == '__main__':
    unittest.main()
