# -*- coding: utf-8 -*-
"""
This is the main entry point for the "questions_lambda" AWS Lambda Function.
Start here when attempting to grok the Lambda code.
"""
import logging
import os

from typing import Dict

import boto3
from botocore.exceptions import ClientError

logger = logging.getLogger(__name__)
logger.setLevel('INFO')

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.getenv('DYNAMO_TABLE', ''))

GREETING_ACTION = 'Welcome'
GREETING_SERVICE = 'GreeterService'
TEST_ESTABLISHMENT = u'testEstablishment'
TEST_CLIENT = u'testClient'
TEST_EMPLOYEE = u'testEmployee'
DEFAULT_GREETING_MSG = u'Welcome to a sample service!'
NON_GREETING_MSG = u'Sorry, can\'t recognize you.'


def get_greeting_msg():
    msg = DEFAULT_GREETING_MSG
    try:
        msg_query_result = table.get_item(
            Key={
                'PK': 'greeting',
                'SK': 'english'
            }
        )
    except ClientError as error:
        logger.error(f'Greeting message query failed, error: {error}')
        return DEFAULT_GREETING_MSG
    if 'Item' not in msg_query_result:
        logger.warning('No greeting message found in database')
        return DEFAULT_GREETING_MSG
    msg = msg_query_result['Item'].get('Greeting', msg)
    return msg


def handle_response(status_code: int, message: str) -> Dict[str, str]:
    return {'statusCode': status_code, 'body': message}


def handler(event: Dict[str, any], context: Dict[str, any]) -> Dict[str, any]:
    """
    The entry point for AWS Lambda Function 'questions_lambda'

    Args:
        event: An event object. https://docs.aws.amazon.com/lambda/latest/dg/python-programming-model-handler-types.html
        context: AWS Lambda context object. https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns: Greeting message
    """
    logger.debug(f'Event: {event}')
    return handle_response(200, get_greeting_msg())
