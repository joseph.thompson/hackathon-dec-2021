#Input variable
variable "bucket_prefix" {}

#Set in terraform/variables/common.vars
variable "app_name" {}
variable "lambda_bucket" {}
variable "team_name" {}
variable "contact_email" {}

#Set in terraform/variables/$ENV.vars
variable "region" {}
variable "region_short" {}
variable "aws_profile" {}
variable "env" {}
variable "role_arn" {}

#GitLab variables
variable "datadog_api_key" {}
variable "datadog_app_key" {}
variable "project_name" {}