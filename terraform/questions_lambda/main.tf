# Creates lambda function and associated security group
module "lambdasvc" {
  app_name     = var.app_name
  team_name       = var.team_name
  contact_email   = var.contact_email
  environment     = var.env
  source          = "../modules/base-app"
  lambda_handler  = "handler"
  lambda_function = "handler"
  bucket_prefix   = var.bucket_prefix
  lambda_bucket   = var.lambda_bucket
  lambda_role_arn = data.terraform_remote_state.lambda-iam.outputs.lambda_role_arn
  sg_description  = "${var.app_name} Security Group"
  region_short    = var.region_short
  lambda_env_variables = {
    "DYNAMO_TABLE" = "${var.env}-ms-${var.app_name}-db"
  }
}

module "datadog_logging" {
  source                                    = "gitlab.com/revelsystems/datadog-lambda/monitor"
  version                                   = "0.1.0-module"
  datadog_api_key                           = var.datadog_api_key
  datadog_app_key                           = var.datadog_app_key
  lambda_function_name                      = lower(local.lambda_name)
  tag_contact                               = var.team_name
  tag_domain                                = local.domain
  tag_repo                                  = local.repo
  enable_lambda_invocation                  = true
  enable_lambda_error                       = true
  enable_lambda_timeout                     = true
  notify_lambda_service_monitor             = true
  lambda_timeout_threshold                  = 70
  lambda_timeout_threshold_warning          = 65
  enhanced_timeouts_threshold               = 70
  enhanced_timeouts_threshold_warning       = 65
  enhanced_error_threshold                  = 10
  enhanced_error_threshold_warning          = 5
  enhanced_invocations_threshold            = 100
  enhanced_invocations_threshold_warning    = 50
  enhanced_init_duration_threshold          = 100
  enhanced_init_duration_threshold_warning  = 50
  lambda_error_threshold                    = 10
  lambda_error_threshold_warning            = 5
  enhanced_outof_memory_threshold           = 10
  enhanced_outof_memory_threshold_warning   = 5
  lambda_invocation_threshold               = 100
  lambda_invocation_threshold_warning       = 50
  enhanced_maxmemory_used_threshold         = 10
  enhanced_maxmemory_used_threshold_warning = 5
}