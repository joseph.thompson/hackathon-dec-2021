locals {
  lambda_name = "${var.app_name}-${var.env}"
  domain      = "Hackathon"
  repo        = var.project_name
}