terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 2.42.0"
    }
    datadog = {
      source  = "Datadog/datadog"
      version = "~> 2.26.0"
    }
  }
  required_version = ">= 1.0.5"
}