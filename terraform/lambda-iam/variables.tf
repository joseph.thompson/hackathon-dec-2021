#Set in terraform/variables/common.vars
variable "app_name" {}
variable "team_name" {}
variable "contact_email" {}
variable "region" {}

#Set in terraform/variables/$ENV.vars
variable "env" {}
variable "role_arn" {}