# Creates role and policy for lambda function
module "lambda-iam" {
  source        = "../modules/lambda-iam"
  lambda_name   = "${var.app_name}-${var.env}"
  team_name     = var.team_name
  contact_email = var.contact_email
  actions = [
    "logs:CreateLogStream",
    "logs:CreateLogGroup",
    "logs:PutLogEvents",
    "ec2:DescribeSecurityGroups",
    "ec2:DescribeSubnets",
    "ec2:DescribeVpcs",
    "ec2:CreateNetworkInterface",
    "ec2:DescribeNetworkInterfaces",
    "ec2:DeleteNetworkInterface",
    "dynamodb:DescribeTable",
    "dynamodb:Query",
    "dynamodb:Scan",
    "dynamodb:GetItem",
    "dynamodb:PutItem",
    "dynamodb:UpdateItem",
    "dynamodb:DeleteItem"
  ]
  resources = [
    "*",
  ]
}
