# Creates role for lambda execution. Version below refers to the date of the policy format.
resource "aws_iam_role" "lambda_role" {
  name               = "${var.lambda_name}-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF


  tags = {
    Domain      = var.team_name
    Service     = var.lambda_name
    Contact     = var.contact_email
  }
}

data "aws_iam_policy_document" "lambda_policy_doc" {
  statement {
    actions = var.actions
    resources = var.resources
    effect = "Allow"
  }
}

resource "aws_iam_policy" "lambda_policy" {
  name   = "${var.lambda_name}-policy"
  policy = data.aws_iam_policy_document.lambda_policy_doc.json
}

resource "aws_iam_role_policy_attachment" "lambda_role_policy_attachment" {
  role       = aws_iam_role.lambda_role.id
  policy_arn = aws_iam_policy.lambda_policy.arn
}

