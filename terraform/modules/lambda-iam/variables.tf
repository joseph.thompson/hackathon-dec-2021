variable "lambda_name" {}
variable "team_name" {}
variable "contact_email"{}

variable "actions" {
  type = list(string)
}

variable "resources" {
  type = list(string)
}