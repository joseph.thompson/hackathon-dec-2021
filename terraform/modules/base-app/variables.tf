variable "lambda_runtime" {
  default = "python3.8"
}

variable "lambda_timeout" {
  type = number
  default = 300
}

variable "lambda_memory_size" {
  type = number
  default = 1024
}

variable "sg_description" {
  default = ""
}

variable "lambda_env_variables" {
  type = map(string)
  default = null
}

variable "lambda_subnets" {
  type = list(string)
  default = null
}

variable "vpc_id" {
  type = string
  default = null
}

variable "log_retention_in_days" {
  type = number
  default = 7
}

variable "lambda_bucket" {}
variable "environment" {}
variable "app_name" {}
variable "bucket_prefix" {}
variable "lambda_role_arn" {}
variable "lambda_handler" {}
variable "lambda_function" {}
variable "team_name" {}
variable "contact_email"{}
variable "region_short" {}