resource "aws_dynamodb_table" "lambda-table-dev" {
  name           = "${var.environment}-ms-${var.app_name}-db"
  hash_key       = "PK"
  range_key      = "SK"
  billing_mode   = "PAY_PER_REQUEST"

  attribute {
    name = "PK"
    type = "S"
  }

  attribute {
    name = "SK"
    type = "S"
  }

  tags = {
    Environment = var.environment
    Domain      = var.team_name
    Service     = var.app_name
    Contact     = var.contact_email
  }
}