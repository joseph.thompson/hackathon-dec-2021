locals {
    function_name = "${var.app_name}-${var.environment}-${var.lambda_handler}"
    file_key = "${var.app_name}/${var.bucket_prefix}/${var.app_name}"
    gateway_name = "${var.environment}-${var.app_name}"
}