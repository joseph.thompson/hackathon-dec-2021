resource "aws_api_gateway_rest_api" "lambda-api-gateway" {
  name        = local.gateway_name
  description = "${var.app_name} API Gateway"
  body        = templatefile("${path.module}/dev-ms-lambda-example-swagger-apigateway.yaml", {
    title       = local.gateway_name
    environment = var.environment
    invoke_arn  = aws_lambda_function.lambda.invoke_arn
  })

  tags = {
    Environment = var.environment
    Domain      = var.team_name
    Service     = var.app_name
    Contact     = var.contact_email
  }
}

resource "aws_api_gateway_deployment" "lambda-api-gateway-deployment" {
  rest_api_id = aws_api_gateway_rest_api.lambda-api-gateway.id
  stage_name  =var.environment
}

# This allows API gateway to access lambda function
resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowAuthSvcInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${var.app_name}-${var.environment}-handler"
  principal     = "apigateway.amazonaws.com"

  # The /*/*/* part allows invocation from any stage, method and resource path
  # within API Gateway REST API.
  source_arn = "${aws_api_gateway_rest_api.lambda-api-gateway.execution_arn}/*/*/*"
}