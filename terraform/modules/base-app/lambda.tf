resource "aws_security_group" "lambda-sg" {
  name        = "${var.app_name}-sg-${var.environment}"
  description = var.sg_description
  vpc_id      = var.vpc_id

  # Allow everything out
  egress {
    cidr_blocks = [
      "0.0.0.0/0",
    ]

    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
  }

  tags = {
    Name        = "sg-${var.region_short}-${var.environment}-${var.app_name}"
    Environment = var.environment
    Domain      = var.team_name
    Service     = var.app_name
    Contact     = var.contact_email
  }
}

data "aws_s3_bucket_object" "jar_hash" {
  bucket = var.lambda_bucket
  key    = "${local.file_key}.zip.sha256"
}

resource "aws_lambda_function" "lambda" {
  s3_bucket        = var.lambda_bucket
  s3_key           = "${local.file_key}.zip"
  function_name    = local.function_name
  source_code_hash = trimspace(data.aws_s3_bucket_object.jar_hash.body)
  role             = var.lambda_role_arn
  handler          = "${var.lambda_handler}.${var.lambda_function}"
  runtime          = var.lambda_runtime
  timeout          = var.lambda_timeout
  memory_size      = var.lambda_memory_size
  # This configuration only applies if lambda needs to run inside VPC
  vpc_config {
    security_group_ids = var.vpc_id == null ? [] : [
      aws_security_group.lambda-sg.id
    ]
    subnet_ids = var.vpc_id == null ? [] : var.lambda_subnets
  }

  tags = {
    Environment = var.environment
    Domain      = var.team_name
    Service     = var.app_name
    Contact     = var.contact_email
  }

  environment {
    variables = var.lambda_env_variables
  }

  depends_on = [
    aws_cloudwatch_log_group.lambda_log_group
  ]
}

data "aws_cloudformation_stack" "datadog_forwarder" {
  name = "datadog-forwarder"
}

resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name              = "/aws/lambda/${local.function_name}"
  retention_in_days = var.log_retention_in_days
  tags = {
    Environment = var.environment
    Domain      = var.team_name
    Service     = var.app_name
    Contact     = var.contact_email
  }
}


resource "aws_cloudwatch_log_subscription_filter" "wepay_webhook_lambda" {
  name            = "${local.function_name}-log-subscription-filter"
  log_group_name  = aws_cloudwatch_log_group.lambda_log_group.name
  filter_pattern  = ""
  destination_arn = data.aws_cloudformation_stack.datadog_forwarder.outputs["DatadogForwarderArn"]

  depends_on = [aws_cloudwatch_log_group.lambda_log_group]
}