output "api_gateway_url" {
  value = aws_api_gateway_deployment.lambda-api-gateway-deployment.invoke_url
}