#!/usr/bin/env bash

# Builds zip file for lambda deployment
#

# * * * * * * * * * * * *
# DO NOT CHANGE ANYTHING FROM HERE TO END OF PART 1
#
if [ -z "$1" ] || [ -z "$2" ]
then
  echo "DIRECTORY AND ZIP PACKAGE NAME PARAMETERS ARE REQUIRED. USAGE: build.sh {DIR} {PACKAGE_NAME}"
  exit 1
else
  APP_NAME=$1
  PACKAGE_NAME=$2
fi


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/"$APP_NAME
if ![ -d "$DIR" ]
then
  echo "ERROR: DIRECTORY $DIR DOES NOT EXIST"
  exit 1
fi

VERSION=$(git tag -l --points-at HEAD)
LAST_COMMIT=$(git rev-parse HEAD)

echo "PACKAGE >>> Creating temp directory and installing source code ..."
echo
mkdir tmp
echo "NAME: $APP_NAME" >> tmp/manifest.txt
echo "VERSION: $VERSION" >> tmp/manifest.txt
echo "GIT COMMIT: $LAST_COMMIT" >> tmp/manifest.txt
echo "CREATED: $(date)" >> tmp/manifest.txt
cat tmp/manifest.txt
# check if this is a clean repo, ie. no uncommitted changes
git diff --quiet HEAD
if [ $? -ne 0 ]
then
    echo
    echo "//////////////////////////////////////////////////////"
    echo "PACKAGE >>> WARNING: THIS REPO HAS UNCOMMITTED CHANGES"
    echo "//////////////////////////////////////////////////////"
    echo
    PACKAGE_NAME=$PACKAGE_NAME"_UNCLEAN"
    echo "WARNING!! THIS PACKAGE WAS BUILT FROM A REPO WITH UNCOMMITTED CHANGES" >> tmp/manifest.txt
    UNCLEAN=1
fi
#
# END OF PART 1
# * * * * * * * * * * * *


# PROJECT SPECIFIC STEPS GO HERE:
rm -rf tmp
echo "PACKAGE >>> Installing libraries ..."
echo $(python3 --version)
echo $DIR
pip3 install -r $DIR/requirements.txt --target tmp
echo "PACKAGE >>> COPYING SOURCE"
pushd $DIR
rsync -r --exclude={terraform,serverless.yml,venv,tmp} . ../tmp
popd
echo "PACKAGE >>> PROJECT COPYING DONE"

# * * * * * * * * * * * *
# DO NOT CHANGE ANYTHING FROM HERE TO END OF PART 2
#
echo "PACKAGE >>> Creating .zip file $PACKAGE_NAME ..."
echo
pushd tmp
zip -r ../$PACKAGE_NAME .
popd
echo ">>> Cleaning up ..."
echo
rm -rf tmp
echo ">>> Finished."
echo
echo "$PACKAGE_NAME"
# automation should never deploy an unclean package
if [ $UNCLEAN ]
then
    echo
    echo "//////////////////////////////////////////////////////"
    echo "PACKAGE >>> WARNING: THIS REPO HAS UNCOMMITTED CHANGES"
    echo "//////////////////////////////////////////////////////"
    echo
    exit 1
fi
#
# END OF PART 2
# * * * * * * * * * * * *
