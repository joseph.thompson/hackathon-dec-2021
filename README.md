#Lambda-app
Lambda deployment consists of the following steps and scripts used to execute them:
* Lambda source code (handler.py)
* Lambda build script which produces an archive for upload (build.sh)
* Upload step in gitlab-ci uploads to s3
* Deployment script in terraform to extract and deploy lambda source
* Terraform script for API gateway setup to provide endpoints for lambda access
* DynamoDB deployment terraform script example

Example endpoint should be accessible at https://edge.cortex.dev.revelup.com/api/greeter/test

**NOTE: these templates work for many use cases, but make sure that you review all of the code to ensure that the template is meeting your needs. You will probably need to make various changes to the template depending on what you are doing.**
